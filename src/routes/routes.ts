import { Request, Response } from "express";
import { ContactController } from "../controllers/contact.controller";

export class Routes {

    public contactController: ContactController = new ContactController();

    public routes(app): void {
        /** 
         * ROOT
         */
        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'Ciao :)'
                })
            })

        /** 
         * CONTACT
         */
        app.route('/contact')

            // Get all contacts   
            .get(this.contactController.getContacts)

            // Create new contact         
            .post(this.contactController.addNewContact)

        app.route('/contact/:contactId')

            // Get a single contact detail  
            .get(this.contactController.getContactById)

            // Update a contact           
            .put(this.contactController.updateContact)

            // Delete a contact     
            .delete(this.contactController.deleteContact)
    }
}