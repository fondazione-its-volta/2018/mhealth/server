import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";

import MongodbMemoryServer from 'mongodb-memory-server';

import { Routes } from "./routes/routes";

class App {

    public app: express.Application;
    public routeProvider: Routes = new Routes();
    public mongoServer = new MongodbMemoryServer();
    public mongoUrl: string = 'mongodb://localhost/mhealth';

    constructor() {
        this.app = express();
        this.config();
        this.routeProvider.routes(this.app);
        this.mongoSetup(true);
    }

    private config(): void {
        debugger;
        // support application/json type post data
        this.app.use(bodyParser.json());
        // support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
        // support any method from any host and log requests
        this.app.use((req, res, next) => {
            // console.log(`TEST`);
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
            if('OPTIONS' === req.method) {
                res.sendStatus(200);
            } else {
                console.log(`Request: ${req.ip} ${req.method} ${req.url}`);
                next();
            }
        })
    }

    private mongoSetup(isInMemory: boolean = false): void {
        if (!isInMemory) {
            console.log("MongoDB NORMAL");
            mongoose.Promise = global.Promise;
            mongoose.connect(this.mongoUrl);
        }
        else {
            console.log("MongoDB IN MEMORY");
            mongoose.Promise = global.Promise;
            this.mongoServer.getConnectionString().then((mongoUri) => {
                const mongooseOpts = { // options for mongoose 4.11.3 and above
                    autoReconnect: true,
                    reconnectTries: Number.MAX_VALUE,
                    reconnectInterval: 1000,
                };

                mongoose.connect(mongoUri, mongooseOpts);

                mongoose.connection.on('error', (e) => {
                    if (e.message.code === 'ETIMEDOUT') {
                        console.log(e);
                        mongoose.connect(mongoUri, mongooseOpts);
                    }
                    console.log(e);
                });

                mongoose.connection.once('open', () => {
                    console.log(`MongoDB successfully connected to ${mongoUri}`);
                });
            });
        }
    }

}

export default new App().app;