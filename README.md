# mHealth server

## Inizializzazione progetto

Eseguire `npm install`

## Esecuzione del server di sviluppo

Eseguire `npm run dev` ed andare su `http://localhost:3000/`

## Build

Eseguire `npm run build`. I file verranno compilati e salvati nella folder `dist/`